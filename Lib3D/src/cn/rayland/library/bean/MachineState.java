package cn.rayland.library.bean;

import java.util.LinkedHashMap;

/**
 * Created by gw on 2016/1/6.
 */
public class MachineState {
    private MachineTask<?> task;
    private int val;
    private int temper1;
    private int desTemper1;
    private int temper2;
    private int desTemper2;
    private int temper3;
    private int desTemper3;
    private int temper4;
    private int desTemper4;
    private int temper5;
    private int desTemper5;
    private boolean working;
    private int lastlinenum;
    private boolean error;
    private boolean poweroff;
    private int heater1;
    private int heater2;
    private int heater3;
    private int heater4;
    private int heater5;
    private int fan;
    private int light;
    private boolean pausing;
    private boolean xMin;
    private boolean xMax;
    private boolean yMin;
    private boolean yMax;
    private boolean zMin;
    private boolean zMax;
    private boolean aMin;
    private boolean aMax;
    private boolean xEnable;
    private boolean yEnable;
    private boolean zEnable;
    private boolean aEnable;
    private boolean bEnable;
    private boolean cEnable;
    private boolean dEnable;
    private int xPosition;
    private int yPosition;
    private int zPosition;
    private LinkedHashMap<Integer, Integer> targetVoltages = new LinkedHashMap<Integer, Integer>();
    private LinkedHashMap<Integer, Integer> targetTempers = new LinkedHashMap<Integer, Integer>();
    private int speedFactor = 100;

	public int getTargetTemper(int id) {
    	Integer target = targetTempers.get(id);
    	if(target!=null){
    		return target.intValue();
    	}
		return 0;
	}
	
	public void addTargetTemper(int id, int temper){
		this.targetTempers.put(id, temper);
	}
	
	public int getVoltage(int id) {
		Integer target = targetVoltages.get(id);
    	if(target!=null){
    		return target.intValue();
    	}
		return 60;
	}
	
	public void addTargetVoltage(int id, int voltage){
		this.targetVoltages.put(id, voltage);
	}

	public int getSpeedFactor() {
		return speedFactor;
	}

	public void setSpeedFactor(int speedFactor) {
		this.speedFactor = speedFactor;
	}
	
	public MachineTask<?> getTask() {
		return task;
	}
	public void setTask(MachineTask<?> task) {
		this.task = task;
	}
	public int getVal() {
		return val;
	}
	public void setVal(int val) {
		this.val = val;
	}
	public int getTemper1() {
		return temper1;
	}
	public void setTemper1(int temper1) {
		this.temper1 = temper1;
	}
	public int getDesTemper1() {
		return desTemper1;
	}
	public void setDesTemper1(int desTemper1) {
		this.desTemper1 = desTemper1;
	}
	public int getTemper2() {
		return temper2;
	}
	public void setTemper2(int temper2) {
		this.temper2 = temper2;
	}
	public int getDesTemper2() {
		return desTemper2;
	}
	public void setDesTemper2(int desTemper2) {
		this.desTemper2 = desTemper2;
	}
	public int getTemper3() {
		return temper3;
	}
	public void setTemper3(int temper3) {
		this.temper3 = temper3;
	}
	public int getDesTemper3() {
		return desTemper3;
	}
	public void setDesTemper3(int desTemper3) {
		this.desTemper3 = desTemper3;
	}
	public int getTemper4() {
		return temper4;
	}
	public void setTemper4(int temper4) {
		this.temper4 = temper4;
	}
	public int getDesTemper4() {
		return desTemper4;
	}
	public void setDesTemper4(int desTemper4) {
		this.desTemper4 = desTemper4;
	}
	public int getTemper5() {
		return temper5;
	}
	public void setTemper5(int temper5) {
		this.temper5 = temper5;
	}
	public int getDesTemper5() {
		return desTemper5;
	}
	public void setDesTemper5(int desTemper5) {
		this.desTemper5 = desTemper5;
	}
	public boolean isWorking() {
		return working;
	}
	public void setWorking(boolean working) {
		this.working = working;
	}
	public int getLastlinenum() {
		return lastlinenum;
	}
	public void setLastlinenum(int lastlinenum) {
		this.lastlinenum = lastlinenum;
	}
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public boolean isPoweroff() {
		return poweroff;
	}
	public void setPoweroff(boolean poweroff) {
		this.poweroff = poweroff;
	}
	public int getHeater1() {
		return heater1;
	}
	public void setHeater1(int heater1) {
		this.heater1 = heater1;
	}
	public int getHeater2() {
		return heater2;
	}
	public void setHeater2(int heater2) {
		this.heater2 = heater2;
	}
	public int getHeater3() {
		return heater3;
	}
	public void setHeater3(int heater3) {
		this.heater3 = heater3;
	}
	public int getHeater4() {
		return heater4;
	}
	public void setHeater4(int heater4) {
		this.heater4 = heater4;
	}
	public int getHeater5() {
		return heater5;
	}
	public void setHeater5(int heater5) {
		this.heater5 = heater5;
	}
	public int getFan() {
		return fan;
	}
	public void setFan(int fan) {
		this.fan = fan;
	}
	public int getLight() {
		return light;
	}
	public void setLight(int light) {
		this.light = light;
	}
	public boolean isPausing() {
		return pausing;
	}
	public void setPausing(boolean pausing) {
		this.pausing = pausing;
	}
	public boolean isxMin() {
		return xMin;
	}
	public void setxMin(boolean xMin) {
		this.xMin = xMin;
	}
	public boolean isxMax() {
		return xMax;
	}
	public void setxMax(boolean xMax) {
		this.xMax = xMax;
	}
	public boolean isyMin() {
		return yMin;
	}
	public void setyMin(boolean yMin) {
		this.yMin = yMin;
	}
	public boolean isyMax() {
		return yMax;
	}
	public void setyMax(boolean yMax) {
		this.yMax = yMax;
	}
	public boolean iszMin() {
		return zMin;
	}
	public void setzMin(boolean zMin) {
		this.zMin = zMin;
	}
	public boolean iszMax() {
		return zMax;
	}
	public void setzMax(boolean zMax) {
		this.zMax = zMax;
	}
	public boolean isxEnable() {
		return xEnable;
	}
	public void setxEnable(boolean xEnable) {
		this.xEnable = xEnable;
	}
	public boolean isyEnable() {
		return yEnable;
	}
	public void setyEnable(boolean yEnable) {
		this.yEnable = yEnable;
	}
	public boolean iszEnable() {
		return zEnable;
	}
	public void setzEnable(boolean zEnable) {
		this.zEnable = zEnable;
	}
	public boolean isaEnable() {
		return aEnable;
	}
	public void setaEnable(boolean aEnable) {
		this.aEnable = aEnable;
	}
	public boolean isbEnable() {
		return bEnable;
	}
	public void setbEnable(boolean bEnable) {
		this.bEnable = bEnable;
	}
	public boolean iscEnable() {
		return cEnable;
	}
	public void setcEnable(boolean cEnable) {
		this.cEnable = cEnable;
	}
	public boolean isdEnable() {
		return dEnable;
	}
	public void setdEnable(boolean dEnable) {
		this.dEnable = dEnable;
	}
	public int getxPosition() {
		return xPosition;
	}
	public void setxPosition(int xPosition) {
		this.xPosition = xPosition;
	}
	public int getyPosition() {
		return yPosition;
	}
	public void setyPosition(int yPosition) {
		this.yPosition = yPosition;
	}
	public int getzPosition() {
		return zPosition;
	}
	public void setzPosition(int zPosition) {
		this.zPosition = zPosition;
	}

	public boolean isaMin() {
		return aMin;
	}

	public void setaMin(boolean aMin) {
		this.aMin = aMin;
	}

	public boolean isaMax() {
		return aMax;
	}

	public void setaMax(boolean aMax) {
		this.aMax = aMax;
	}

    
}
