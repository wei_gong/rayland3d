package cn.rayland.rayland3d.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import cn.rayland.library.utils.MachineController;
import cn.rayland.rayland3d.R;

/**
 * Created by gw on 2016/3/28.
 */
public class ControlActivity extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {
    private Button stop, x_left, x_right, y_forward, y_backward, z_up, z_down, in_filament, out_filament;
    private MachineController controller;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        initView();
        controller = MachineController.getInstance(this);
    }

    private void initView() {
        stop = (Button) findViewById(R.id.stop);
        x_left = (Button) findViewById(R.id.x_left);
        x_right = (Button) findViewById(R.id.x_right);
        y_forward = (Button) findViewById(R.id.y_forward);
        y_backward = (Button) findViewById(R.id.y_backward);
        z_up = (Button) findViewById(R.id.z_up);
        z_down = (Button) findViewById(R.id.z_down);
        in_filament = (Button) findViewById(R.id.in_filament);
        out_filament = (Button) findViewById(R.id.out_filament);
        stop.setOnTouchListener(this);
        x_left.setOnTouchListener(this);
        x_right.setOnTouchListener(this);
        y_forward.setOnTouchListener(this);
        y_backward.setOnTouchListener(this);
        z_up.setOnTouchListener(this);
        z_down.setOnTouchListener(this);
        in_filament.setOnClickListener(this);
        out_filament.setOnClickListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                switch (v.getId()){
                    case R.id.x_left:
                        controller.xLeft();
                        break;
                    case R.id.x_right:
                        controller.xRight();
                        break;
                    case R.id.y_forward:
                        controller.yForward();
                        break;
                    case R.id.y_backward:
                        controller.yBackForward();
                        break;
                    case R.id.z_up:
                        controller.zUp();
                        break;
                    case R.id.z_down:
                        controller.zDown();
                        break;
                }
                break;
            case MotionEvent.ACTION_UP:
                controller.stop();
                break;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.in_filament:
                controller.inFilament();
                startActivity(new Intent(ControlActivity.this, StateActivity.class));
                break;
            case R.id.out_filament:
                controller.outFilament();
                startActivity(new Intent(ControlActivity.this, StateActivity.class));
                break;
            case R.id.stop:
                controller.stop();
                break;
        }
    }
}
