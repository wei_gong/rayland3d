package cn.rayland.api;

public class IndependentOutput {
	public int temper_control_type;
	public int temper_control_position;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndependentOutput other = (IndependentOutput) obj;
		if (temper_control_position != other.temper_control_position)
			return false;
		if (temper_control_type != other.temper_control_type)
			return false;
		return true;
	}
	
	
}
