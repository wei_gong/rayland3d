package cn.rayland.api;


public interface SliceCallback {
	
	void onProgress(String type, int current, int total, float percent);
	
}
