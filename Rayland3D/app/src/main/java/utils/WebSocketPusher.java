package utils;

import android.util.Log;

import com.alibaba.fastjson.JSON;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.Timer;
import java.util.TimerTask;
import cn.rayland.library.utils.MachineManager;

/**
 * Created by gw on 2016/5/3.
 */
public class WebSocketPusher {
    private static final String TAG = WebSocketPusher.class.getSimpleName();
    private static WebSocketClient client;
    private static Timer timer;
    private static TimerTask task;
    private static PushData data = new PushData();
    public static void startPush(final String url, final long delay, final MachineManager manager) {
        new Thread(){
            @Override
            public void run() {
                try {
                    timer = new Timer();
                    URI uri = new URI(url);
                    task = new TimerTask() {
                        @Override
                        public void run() {
                            data.setProgress(manager.getMachineState().getTask().getProgress());
                            data.setTemperature(manager.getMachineState().getTemper());
                            data.setTime(manager.getMachineState().getTask().getNeedTime());
                            client.send(JSON.toJSONString(data));
                            Log.d(TAG, "send" + JSON.toJSONString(data));
                        }
                    };

                    client = new WebSocketClient(uri, new Draft_17()) {
                        @Override
                        public void onOpen(ServerHandshake handshakedata) {
                            Log.d(TAG, "Opened");
                            timer.schedule(task, 0, delay);
                        }

                        @Override
                        public void onMessage(String message) {
                            Log.d(TAG, "onMessage = " + message);
                        }

                        @Override
                        public void onClose(int code, String reason, boolean remote) {
                            Log.d(TAG, "Closed");
                            timer.cancel();
                        }

                        @Override
                        public void onError(Exception ex) {
                            Log.i(TAG, "Error " + ex.getMessage());
                            timer.cancel();
                        }
                    };
                    client.connect();
                    Log.d(TAG, "Try connect");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }

    public static void stopPush() {
        new Thread(){
            @Override
            public void run() {
                try {
                    if (null != client) {
                        timer.cancel();
                        client.send("Print is over");
                        Log.d(TAG, "send" +"Print is over");
                        client.close();
                        Log.d(TAG, "Try close");
                        client = null;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static class PushData{
        private int temperature;
        private int progress;
        private float x;
        private float y;
        private float z;
        private float time;

        public int getTemperature() {
            return temperature;
        }

        public void setTemperature(int temperature) {
            this.temperature = temperature;
        }

        public int getProgress() {
            return progress;
        }

        public void setProgress(int progress) {
            this.progress = progress;
        }

        public float getX() {
            return x;
        }

        public void setX(float x) {
            this.x = x;
        }

        public float getY() {
            return y;
        }

        public void setY(float y) {
            this.y = y;
        }

        public float getZ() {
            return z;
        }

        public void setZ(float z) {
            this.z = z;
        }

        public float getTime() {
            return time;
        }

        public void setTime(float time) {
            this.time = time;
        }
    }
}
