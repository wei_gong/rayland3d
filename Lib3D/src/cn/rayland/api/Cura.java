package cn.rayland.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.os.Build;


public class Cura {
	public static native byte[] slice(String[] command, SliceCallback callback);
	
	static{
//		String CPU_ABI = Build.CPU_ABI;
//		try {
//			InputStream in = Cura.class.getResourceAsStream("/" + CPU_ABI
//					+ "/libCuraEngine.so");
//			File f = File.createTempFile("JNI-", "Temp");
//			FileOutputStream out = new FileOutputStream(f);
//			byte[] buf = new byte[1024];
//			int len;
//			while ((len = in.read(buf)) > 0) {
//				out.write(buf, 0, len);
//			}
//			in.close();
//			out.close();
//			System.load(f.getAbsolutePath());
//			f.delete();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		System.loadLibrary("CuraEngine");
	}
	
}
