package cn.rayland.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class DlpSlicer {

	private float layerThickness;
	private File stlFile;
	
	public void slice(){
		List<Facet> facetList = new ArrayList<>();
		if(isTextFile(stlFile)){
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(stlFile));
				String line;
				Facet facet = null;
				Vertex vertex = null;
				int index = 0;
				float zMin = 0;
				float zMax = 0;
				while((line=br.readLine())!=null){
					line = line.trim();
					if(line.startsWith("facet normal")){
						facet = new Facet();
						index = 0;
					}else if(line.startsWith("vertex")){
						line.replace("(^vertex)([ \f\r\t\n]+)", "");
						String[] values = line.split(" ");
						if(values.length == 3){
							vertex = new Vertex();
							vertex.x = Float.parseFloat(values[0]);
							vertex.y = Float.parseFloat(values[1]);
							vertex.z = Float.parseFloat(values[2]);
							
							facet.vertexs[index] = vertex;
							facet.zMin = facet.zMin > vertex.z ? vertex.z : facet.zMin;
							facet.zMax = facet.zMax < vertex.z ? vertex.z : facet.zMax;
							index ++;
							
							if(index == 3){
								facetList.add(facet);
								zMin = zMin > facet.zMin ? facet.zMin : zMin;
								zMax = zMax < facet.zMax ? facet.zMax : zMax;
							}
						}
					}
				}
				facetList.sort(new Comparator<Facet>() {
					@Override
					public int compare(Facet o1, Facet o2) {
						if(o1.zMin < o2.zMin){
							return -1;
						}else if(o1.zMin == o2.zMin){
							if(o1.zMax < o2.zMax){
								return -1;
							}
						}
						return 1;
					}
				});
				for(float i=zMin;i<zMax;i+=layerThickness){
					for(Facet facet2 : facetList){
						if(facet2.zMin > i || facet2.zMax < i){
							break;
						}
						Vertex v0 = null;
						Vertex v1 = null;
						Vertex v2 = null;
						for(Vertex vertex2 : facet2.vertexs){
							if(vertex2.z == facet2.zMin){
								v0 = vertex2;
							}else if(vertex2.z == facet2.zMax){
								v2 = vertex2;
							}else{
								v1 = vertex2;
							}
						}
						if(v0!=null && v1!=null && v2!=null){
							Vertex sv1 = new Vertex();
							Vertex sv2 = new Vertex();
								//x = [(z-z0)x1 + (z1-z)x0] / (z1-z0);
								//y = [(z-z0)y1 + (z1-z)y0] / (z1-z0);
								//z = i;
								sv1.x = ((i-v0.z)*v1.x + (v1.z-i)*v0.x) / (v1.z-i);
								sv1.y = ((i-v0.z)*v1.y + (v1.z-i)*v0.y) / (v1.z-i);
								sv1.z = i;
								sv2.x = ((i-v0.z)*v2.x + (v2.z-i)*v0.x) / (v2.z-i);
								sv2.y = ((i-v0.z)*v2.y + (v2.z-i)*v0.y) / (v2.z-i);
								sv2.z = i;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			
		}
	}
	
	
	private static boolean isTextFile(File stlFile){
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(stlFile));
            br.skip(80);
            int line = 0;
            String buffer;
            while((buffer = br.readLine()) != null && line < 5){
                line ++;
                if(buffer.contains("facet")||buffer.contains("outer")||buffer.contains("vertex")||buffer.contains("end")){
                    return true;
                }
            }
            br.close();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
	
	public static class Facet {
		Vertex[] vertexs = new Vertex[3];
		float zMin;
		float zMax;
	}
	
	public static class Vertex {
		float x;
		float y;
		float z;
	}
}
