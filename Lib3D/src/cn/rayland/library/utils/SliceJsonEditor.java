package cn.rayland.library.utils;

import java.util.Locale;
import org.json.JSONObject;
import android.util.Log;
import cn.rayland.library.bean.SliceSetting;
/**
 * modify fdmprinter.json according to slice setting
 * @author gw
 *
 */
public class SliceJsonEditor {

	private static final String TAG = SliceJsonEditor.class.getSimpleName();
	
	public static void editSliceJson(String jsonString, String destPath, String startCode, SliceSetting setting){
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			
			JSONObject machine_settingsJsonObject = jsonObject.getJSONObject("machine_settings");
			JSONObject machine_start_gcode = machine_settingsJsonObject.getJSONObject("machine_start_gcode");
			machine_start_gcode.put("default", startCode);
			
			JSONObject categories = jsonObject
					.getJSONObject("categories");
			// set support type
			String supportValue = setting.getSupport().toLowerCase(Locale.ENGLISH);
			JSONObject support_settings = categories.getJSONObject(
					"support").getJSONObject("settings");
			if(supportValue.equals("none")||supportValue.equals("0")){
				support_settings.getJSONObject("support_enable").put(
						"default", false);
			}else{
				support_settings.getJSONObject("support_enable").put(
						"default", true);
				if (supportValue.equals("everywhere")) {
					support_settings.getJSONObject("support_type").put(
							"default", "everywhere");
				} else {
					support_settings.getJSONObject("support_type").put(
							"default", "buildplate");
				}
			}
			
			//set adhesion type
			String adhesionValue = setting.getAdhesion().toLowerCase(Locale.ENGLISH);
			JSONObject adhesion_type = categories
					.getJSONObject("platform_adhesion")
					.getJSONObject("settings")
					.getJSONObject("adhesion_type");
			adhesion_type.put("default", adhesionValue);

			// set print speed
			int speedValue = setting.getSpeed();
			JSONObject speed_print = categories.getJSONObject("speed").getJSONObject("settings").getJSONObject("speed_print");
			speed_print.put("default", speedValue);
			//set infill speed 
			speed_print.getJSONObject("children").getJSONObject("speed_infill").put("default", speedValue+6);
			//set wall speed, half of print speed
			JSONObject speed_wall = speed_print.getJSONObject("children").getJSONObject("speed_wall");
			speed_wall.put("default", speedValue/2);
			speed_wall.getJSONObject("children").getJSONObject("speed_wall_0").put("default", (speedValue/2));
			speed_wall.getJSONObject("children").getJSONObject("speed_wall_x").put("default", (speedValue/2) + 6);
			//set top bottom speed
			speed_print.getJSONObject("children").getJSONObject("speed_topbottom").put("default", speedValue/2);
			//set support speed
			JSONObject speed_support = speed_print.getJSONObject("children").getJSONObject("speed_support");
			speed_support.put("default", speedValue+6);
			speed_support.getJSONObject("children").getJSONObject("speed_support_lines").put("default", speedValue+6);
			speed_support.getJSONObject("children").getJSONObject("speed_support_roof").put("default", speedValue);
			
			//set travel speed
			categories.getJSONObject("speed").getJSONObject("settings").getJSONObject("speed_travel").put("default", speedValue+20);
			//set layer0 speed
			categories.getJSONObject("speed").getJSONObject("settings").getJSONObject("speed_layer_0").put("default", speedValue/2);

			// set layer_height
			float layerHeightValue = setting.getLayerThickness();
			JSONObject layer_height = categories
					.getJSONObject("resolution")
					.getJSONObject("settings")
					.getJSONObject("layer_height");
			layer_height.put("default", layerHeightValue);

			// set print temperature
			int printTemperatureValue = setting.getTemp();
			JSONObject material_print_temperature = categories
					.getJSONObject("material")
					.getJSONObject("settings")
					.getJSONObject("material_print_temperature");
			material_print_temperature.put("default",
					printTemperatureValue);

			// set retrac
			float retractLengthValue = setting.getRetrac_length();
			JSONObject retract_length = categories
					.getJSONObject("material")
					.getJSONObject("settings")
					.getJSONObject("retraction_amount");
			retract_length.put("default", retractLengthValue);
			
			//set top bottom layers according layer height
			JSONObject top_bottom_thickness = categories.getJSONObject("shell").getJSONObject("settings")
														.getJSONObject("shell_thickness")
														.getJSONObject("children")
														.getJSONObject("top_bottom_thickness");
			JSONObject top_thickness = top_bottom_thickness.getJSONObject("children").getJSONObject("top_thickness");
			float topThicknessValue = (float) top_thickness.getDouble("default");
			int topLayersValue = (int) Math.ceil(topThicknessValue/layerHeightValue);
			top_thickness.getJSONObject("children").getJSONObject("top_layers").put("default", topLayersValue);
			
			JSONObject bottom_thickness = top_bottom_thickness.getJSONObject("children").getJSONObject("bottom_thickness");
			float bottomThicknessValue = (float) bottom_thickness.getDouble("default");
			int bottomLayersValue = (int) Math.ceil(bottomThicknessValue/layerHeightValue);
			bottom_thickness.getJSONObject("children").getJSONObject("bottom_layers").put("default", bottomLayersValue);
			
			IOUtils.save2File(jsonObject.toString(3).getBytes(),
					destPath.substring(0, destPath.lastIndexOf("/")), destPath.substring(destPath.lastIndexOf("/")+1));
			
			Log.i(TAG, "new slice json create success");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
