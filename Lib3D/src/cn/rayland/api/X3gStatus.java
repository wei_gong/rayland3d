package cn.rayland.api;

/**
 * Created by Lei on 2/4/2016.
 */
public class X3gStatus {
    public float time;
    public float length;
    public X3gStatus(float thisTime,float thisLength){
        time = thisTime;
        length = thisLength;
    }
}
