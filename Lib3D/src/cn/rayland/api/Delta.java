package cn.rayland.api;

public class Delta {

	public double diagonal_rod;
	public double smooth_rod_offset;
	public double effector_offset;
	public double carriage_offset;
	public double container_height;
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Delta other = (Delta) obj;
		if (Double.doubleToLongBits(carriage_offset) != Double
				.doubleToLongBits(other.carriage_offset))
			return false;
		if (Double.doubleToLongBits(container_height) != Double
				.doubleToLongBits(other.container_height))
			return false;
		if (Double.doubleToLongBits(diagonal_rod) != Double
				.doubleToLongBits(other.diagonal_rod))
			return false;
		if (Double.doubleToLongBits(effector_offset) != Double
				.doubleToLongBits(other.effector_offset))
			return false;
		if (Double.doubleToLongBits(smooth_rod_offset) != Double
				.doubleToLongBits(other.smooth_rod_offset))
			return false;
		return true;
	}
	
	
	
	
}
