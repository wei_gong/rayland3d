package cn.rayland.api;

/**
 * Created by Lei on 2/4/2016.
 */
public class Axis {
    public double max_feedrate;
    public double home_feedrate;
    public double steps_per_mm;
    public double length;
    public double max_acc;
    public double max_speed_change;
    public double cornerPos;
    public int endstop;
    public int endstopPol;
    public int dir;
    public int driving_voltage;

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Axis other = (Axis) obj;
		if (Double.doubleToLongBits(cornerPos) != Double
				.doubleToLongBits(other.cornerPos))
			return false;
		if (dir != other.dir)
			return false;
		if (driving_voltage != other.driving_voltage)
			return false;
		if (endstop != other.endstop)
			return false;
		if (endstopPol != other.endstopPol)
			return false;
		if (Double.doubleToLongBits(home_feedrate) != Double
				.doubleToLongBits(other.home_feedrate))
			return false;
		if (Double.doubleToLongBits(length) != Double
				.doubleToLongBits(other.length))
			return false;
		if (Double.doubleToLongBits(max_acc) != Double
				.doubleToLongBits(other.max_acc))
			return false;
		if (Double.doubleToLongBits(max_feedrate) != Double
				.doubleToLongBits(other.max_feedrate))
			return false;
		if (Double.doubleToLongBits(max_speed_change) != Double
				.doubleToLongBits(other.max_speed_change))
			return false;
		if (Double.doubleToLongBits(steps_per_mm) != Double
				.doubleToLongBits(other.steps_per_mm))
			return false;
		return true;
	}

    
}
