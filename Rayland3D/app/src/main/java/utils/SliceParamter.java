package utils;

/**
 * Created by gw on 2016/5/12.
 */
public class SliceParamter {
    public static final int DEFAULT_SPEED = 20;
    public static final float DEFAULT_SLICEHEIGHT = 0.1f;
    public static final String DEFAULT_ENGINE = "cura";
    public static final String LOCAL_ENGINE = "cura(local)";
    public static final float DEFAULT_RETRAC_LENGTH = 4.0f;
    public static final int DEFAULT_TEMPER = 210;
    public static final String DEFAULT_CURA_SUPPORT = "None";
}
