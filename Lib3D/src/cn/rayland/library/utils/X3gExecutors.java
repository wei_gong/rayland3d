package cn.rayland.library.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.concurrent.Semaphore;
import cn.rayland.api.Gpx;
import cn.rayland.api.X3gStatus;
import cn.rayland.library.bean.FileTask;
import cn.rayland.library.bean.MachineState;
import cn.rayland.library.bean.MachineTask;
import cn.rayland.library.sqlite.SQLiteHelper;
import cn.rayland.library.stm32.IIC;
import cn.rayland.library.utils.ConvertUtils.Callback;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.MessageQueue.IdleHandler;
/**
 * Created by gw on 2015-08-22.
 */
public class X3gExecutors {
	public static final String TAG = X3gExecutors.class.getSimpleName();
	private static final byte[] PAUSE = new byte[]{2, 0};
	private static final byte[] STOP = new byte[] { 1, 0 };
	private static final byte[] REBOOT = new byte[] { 16, 0 };
	private static final byte[] CLEAR_LINE_NUM = new byte[] { 0, 4 };
	private static byte[] data = new byte[30];
	private static IIC stm32 = IIC.getInstance();
	private static Handler uiHandler;
	private static Handler taskHandler;
	private static Handler x3gHandler;
	private static volatile boolean isCancel;
	private static volatile int lastLineOverNum; 
	private static int oldLastLineNum;
	public static volatile MachineState state;
	private static MachineManager manager;
	public static SQLiteHelper sqliteHelper;

	public static void init(MachineManager machineManager) {
		if (stm32 == null) {
			LogUtil.e(TAG, "stm32 not found");
			return;
		}
		if (x3gHandler == null && taskHandler == null) {
			manager = machineManager;
			state = new MachineState();
			sqliteHelper = new SQLiteHelper(manager.context);
			HandlerThread thread = new HandlerThread("sendX3g");
			HandlerThread taskThread = new HandlerThread("sendTask");
			taskThread.start();
			thread.start();
			uiHandler = new Handler(Looper.getMainLooper());
			x3gHandler = new Handler(thread.getLooper());
			taskHandler = new Handler(taskThread.getLooper());
			x3gHandler.getLooper().getQueue().addIdleHandler(new IdleHandler() {
				public boolean queueIdle() {
					readData();
					x3gHandler.post(null);
					return true;
				}
			});
		}
	}

	private static void readData() {
		try {
			stm32.readData(30, data);
			if (data != null) {
				state.setVal((data[0]&0XFF) | ((data[1]&0XFF)<<8));
				state.setWorking((data[2] & 1) != 0);
				state.setError((data[2] & (1 << 1)) != 0);
				state.setPoweroff((data[2] & (1 << 3))==0);
				state.setaEnable((data[3] & 1)!=0);
				state.setbEnable((data[3] & (1 << 1))!=0);
				state.setcEnable((data[3] & (1 << 2))!=0);
				state.setdEnable((data[3] & (1 << 3))!=0);
				state.setxEnable((data[3] & (1 << 4))!=0);
				state.setyEnable((data[3] & (1 << 5))!=0);
				state.setzEnable((data[3] & (1 << 6))!=0);
				
				int num = (data[4]&0XFF) | ((data[5]&0XFF)<<8);
				if(oldLastLineNum>(lastLineOverNum*65535)+65530 && num<5){
					lastLineOverNum++;
				}
				state.setLastlinenum((lastLineOverNum*65536)+num);
				oldLastLineNum = state.getLastlinenum();

				state.setFan(Math.round((data[6] & 0xFF)/2.55f));
				state.setLight(Math.round((data[7] & 0xFF)/2.55f));
				state.setHeater1(data[8] & 0XFF);
				state.setHeater2(data[9] & 0XFF);
				state.setHeater3(data[10] & 0XFF);
				state.setHeater4(data[11] & 0XFF);
				state.setHeater5(data[12] & 0XFF);
				try {
					state.setTemper1(Constant.O2C[manager.machine.extruder_a.temper_control_type][(data[13]&0XFF) | ((data[14]&0XFF)<<8)]);
					state.setTemper2(Constant.O2C[manager.machine.extruder_b.temper_control_type][(data[15]&0XFF) | ((data[16]&0XFF)<<8)]);
					state.setTemper3(Constant.O2C[manager.machine.extruder_a.temper_control_type][(data[17]&0XFF) | ((data[18]&0XFF)<<8)]);
					state.setTemper4(Constant.O2C[manager.machine.extruder_b.temper_control_type][(data[19]&0XFF) | ((data[20]&0XFF)<<8)]);
					state.setTemper5(Constant.O2C[manager.machine.heated_bed.temper_control_type][(data[21]&0XFF) | ((data[22]&0XFF)<<8)]);
				} catch (Exception e) {
				}
				if(manager.machine.extruder_a.temper_control_type == 2){
					state.setTemper1((data[13]&0XFF) | ((data[14]&0XFF)<<8));
				}
				if(manager.machine.extruder_b.temper_control_type == 2){
					state.setTemper2((data[15]&0XFF) | ((data[16]&0XFF)<<8));
				}
				
				state.setxPosition((data[23]&0XFF) | ((data[24])<<8));
				state.setyPosition((data[25]&0XFF) | ((data[26])<<8));
				state.setzPosition((data[27]&0XFF) | ((data[28])<<8));
				if(manager.machine.axis_x.endstopPol==0){
					state.setxMin((data[29] & 1)!=0);
					state.setxMax((data[29] & (1 << 1))!=0);
				}else{
					state.setxMin((data[29] & 1)==0);
					state.setxMax((data[29] & (1 << 1))==0);
				}
				if(manager.machine.axis_y.endstopPol==0){
					state.setyMin((data[29] & (1 << 2))!=0);
					state.setyMax((data[29] & (1 << 3))!=0);
				}else{
					state.setyMin((data[29] & (1 << 2))==0);
					state.setyMax((data[29] & (1 << 3))==0);
				}
				if(manager.machine.axis_z.endstopPol==0){
					state.setzMin((data[29] & (1 << 4))!=0);
					state.setzMax((data[29] & (1 << 5))!=0);
				}else{
					state.setzMin((data[29] & (1 << 4))==0);
					state.setzMax((data[29] & (1 << 5))==0);
				}
				state.setaMin((data[29] & (1 << 6))==0);
				state.setaMax((data[29] & (1 << 7))==0);
				
				if (state.isError()) {
					LogUtil.e(TAG, "stm32 error");
					uiHandler.post(new Runnable() {
						public void run() {
							if (state.getTask() != null && state.getTask().getTaskCallback() != null) {
								state.getTask().getTaskCallback().onError();
							}
							state.setTask(null);
							isCancel = true;
							stm32.sendData(STOP);
						}
					});
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
//			if((data[3] & (1 << 3))==0 && !state.isPausing()){
//				execPause();
//			}
//			LogUtil.v(TAG, "TEMP_INDEX = "+Integer.parseInt("000000"+((data[3]&(1<<4))>>4)+""+((data[3]&(1<<3))>>3), 2));
//			LogUtil.v(TAG, "Temp = "+state.getTemper());
			
	}

	public static void execInitBytes() {
		if (taskHandler == null) {
			LogUtil.e(TAG, "X3gExecutors not init");
			return;
		}
		byte[] initX3g = Gpx.getInitX3g();
		byte[] configX3g = Gpx.gpxConverPieceGcode("G130 X"+manager.machine.axis_x.driving_voltage+" Y"+manager.machine.axis_y.driving_voltage+" Z"+manager.machine.axis_z.driving_voltage+" A"+manager.machine.extruder_a.driving_voltage+" B"+manager.machine.extruder_b.driving_voltage+" C"+manager.machine.axis_c.driving_voltage+" D"+manager.machine.axis_d.driving_voltage+"\nG92 X0 Y0 Z0 A0 B0 C0 D0", new X3gStatus(0, 0));
		byte[] datas = new byte[initX3g.length+configX3g.length];
		System.arraycopy(initX3g, 0, datas, 0, initX3g.length);
		System.arraycopy(configX3g, 0, datas, initX3g.length, configX3g.length);
		ByteArrayInputStream bis = new ByteArrayInputStream(datas);
		execStream(null, bis);
	}	
	
	public static void execStream(final MachineTask<?> task,
			final InputStream is) {
		if (taskHandler == null) {
			LogUtil.e(TAG, "X3gExecutors not init");
			return;
		}
		taskHandler.post(new Runnable() {
			public void run() {
				try {
					isCancel = false;
					LogUtil.d(TAG, "task start");
					stm32.sendData(CLEAR_LINE_NUM);
					lastLineOverNum = 0;
					state.setTask(task);
					if (task != null) {
						if(task.getSend()==0 && task.getTaskCallback() != null){
							uiHandler.post(new Runnable() {
								
								@Override
								public void run() {
									task.getTaskCallback().onStart();	
								}
							});
						}

						sqliteHelper.deleteTask();
						is.skip(task.getSend());
					}
					byte[] bytes = new byte[256];
					bytes[0] = bytes[1] = 0;
					int length = -1;
					int index = 0;
					long position = task!=null ? task.getSend(): 0;
					while (!isCancel) {
						if ((length = X3gParser.readCommand(is, bytes, 2)) != -1) {
							if(state.getTask()!=null){
								state.getTask().addIndex(index, position);
							}
							index ++;
							position += length;
							execBlockedBytes(bytes, 0, length+2);
						}else{
//							if(task instanceof GcodeTask){
//								isCancel = true;
//								uiHandler.post(new Runnable() {
//									public void run() {
//										if(state.getTask()!= null && state.getTask().getTaskCallback()!=null){
//											state.getTask().getTaskCallback().onFinished();
//										}
//										state.setTask(null);
//									}
//								});
//							}else{
								readData();
								if(!state.isWorking()){
									isCancel = true;
									uiHandler.post(new Runnable() {
										public void run() {
											if(state.getTask()!= null && state.getTask().getTaskCallback()!=null){
												state.getTask().getTaskCallback().onFinished();
											}
											state.setTask(null);
										}
									});
								}
//							}
						}
					}
					bytes = null;
					LogUtil.d(TAG, "task end");
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						is.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}
	
	public static void execGcode(final String gcode){
		if (x3gHandler == null) {
			LogUtil.e(TAG, "X3gExecutors not init");
			return;
		}
		isCancel = false;	
		ConvertUtils.gcodeToX3g(gcode, new Callback<byte[]>() {

			public void onPreConvert() {

			}

			public void onConvertSuccess(final byte[] result, X3gStatus x3gStatus) {
				x3gHandler.post(new Runnable() {
					public void run() {
						ByteArrayInputStream bis = new ByteArrayInputStream(result);
						byte[] bytes = new byte[256];
						bytes[0] = bytes[1] = 0;
						int length = -1;
						while (!isCancel && !state.isError() && (length = X3gParser.readCommand(bis, bytes, 2)) != -1) {
							while (!isCancel && !state.isError()) {
								readData();
								if (state.getVal() < length) {
									continue;
								}
								stm32.sendData(bytes, 0, length+2);
								printHexString(bytes, 2, length);
								break;
							}
						}
						try {
							bis.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
			}

			public void onConvertProgress(String type, int progress) {

			}

			public void onConvertFailed(String error) {

			}
		});
	}
	
	public static void execBlockedGcode(final String gcode){
		if (x3gHandler == null) {
			LogUtil.e(TAG, "X3gExecutors not init");
			return;
		}
		final Semaphore semaphore = new Semaphore(0);
			
				ConvertUtils.gcodeToX3g(gcode, new Callback<byte[]>() {
					
					public void onPreConvert() {
						
					}
					
					public void onConvertSuccess(byte[] result, X3gStatus x3gStatus) {
						execInsertBytes(result);
						semaphore.release();
					}
					
					public void onConvertProgress(String type, int progress) {
						
					}
					
					public void onConvertFailed(String error) {
						
					}
				});
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void execBlockedBytes(final byte[] bytes, final int offset,
			final int length) {
		if (x3gHandler == null) {
			LogUtil.e(TAG, "X3gExecutors not init");
			return;
		}
		final Semaphore semaphore = new Semaphore(0);
		x3gHandler.post(new Runnable() {
			public void run() {
				while (!isCancel && !state.isError()) {
					readData();
					if (state.getVal() < length) {
						continue;
					}
					if(!isCancel && !state.isError()){
						stm32.sendData(bytes, 0, length);
						printHexString(bytes, 2, length-2);
						if(bytes.length>=3 && (bytes[2]&0xff) == 145){
							state.addTargetVoltage(bytes[3]&0XFF, bytes[4]&0xff);
						}
						if(bytes.length>=5 && (bytes[2]&0xff) == 136 && (bytes[4]&0xff) == 3){
							state.addTargetTemper(bytes[3]&0XFF, (bytes[6]&0XFF) | ((bytes[7]&0XFF)<<8));
							LogUtil.e(TAG, "target "+(bytes[3]&0XFF)+" temper = "+state.getTargetTemper(bytes[3]&0XFF));
						}
						if(bytes.length>=3 && (bytes[2]&0xff) == 165){
							state.setSpeedFactor((bytes[3]&0XFF) | ((bytes[4]&0XFF)<<8));
							LogUtil.e(TAG, "speed factor = "+state.getSpeedFactor());
						}
						if(state.getTask()!=null){
							state.getTask().setSend(state.getTask().getSend()+length-2);
						}
					}
					break;
				}
				semaphore.release();
			}
		});
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void execInsertBytes(final byte[] bytesData) {
		if (x3gHandler == null) {
			LogUtil.e(TAG, "X3gExecutors not init");
			return;
		}
		x3gHandler.postAtFrontOfQueue(new Runnable() {
			public void run() {
				ByteArrayInputStream bis = new ByteArrayInputStream(bytesData);
				byte[] bytes = new byte[256];
				bytes[0] = bytes[1] = 0;
				int length = -1;
				while ((length = X3gParser.readCommand(bis, bytes, 2)) != -1 && !state.isError()) {
					while (!state.isError()) {
						readData();
						if (state.getVal() < length) {
							continue;
						}
						stm32.sendData(bytes, 0, length+2);
						LogUtil.d(TAG, "execute insert bytes:");
						printHexString(bytes, 2, length);
						if((bytes[2]&0xff) == 145){
							state.addTargetVoltage(bytes[3]&0XFF, bytes[4]&0xff);
						}
						if((bytes[2]&0xff) == 136 && (bytes[4]&0xff) == 3){
							state.addTargetTemper(bytes[3]&0XFF, (bytes[6]&0XFF) | ((bytes[7]&0XFF)<<8));
							LogUtil.e(TAG, "target "+(bytes[3]&0XFF)+" temper = "+state.getTargetTemper(bytes[3]&0XFF));
						}
						if((bytes[2]&0xff) == 165){
							state.setSpeedFactor((bytes[3]&0XFF) | ((bytes[4]&0XFF)<<8));
							LogUtil.e(TAG, "speed factor = "+state.getSpeedFactor());
						}
						break;
					}
				}
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void execStop() {
		if (stm32 != null) {
			state.setPausing(false);
			sqliteHelper.deleteTask();
			isCancel = true;
			stm32.sendData(STOP);
//			state = new MachineState();
			LogUtil.d(TAG, "stm32 stop");
			uiHandler.post(new Runnable() {
				public void run() {
					if (state.getTask() != null
							&& state.getTask().getTaskCallback() != null) {
						state.getTask().getTaskCallback().onCancel();
					}
					state.setTask(null);
				}
			});
		}
	}

	/**
	 * pause print, remember lastlinenum and position, move extruder away
	 */
	public static void execPause(){	
		if(stm32 != null && state.getTask()!=null && state.getTask() instanceof FileTask && !state.isPausing()){
			state.setPausing(true);
			isCancel = true;
			stm32.sendData(PAUSE);
			LogUtil.d(TAG, "stm32 pause");
			saveState();
		}
	}
	/**
	 * resume print, move extruder back to continue printing
	 */
	public static void execResume(){
		try {
			state.setPausing(false);
			if(state.getTask()!=null){
				execStream(state.getTask(), new FileInputStream((File)state.getTask().getContent()));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void execReboot() {
		if (stm32 != null) {
			stm32.sendData(REBOOT);
			LogUtil.d(TAG, "stm32 reboot");
		}
	}

	public static void printHexString(byte[] b, int offset, int lenth) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < lenth; i++) {
			String hex = Integer.toHexString(b[i+offset] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			buffer.append(hex+" ");
		}
		LogUtil.d(TAG, buffer.toString());
	}
	
	/**
	 * save task state to sqlitedatabase
	 */
	public static void saveState() {
		taskHandler.post(new Runnable() {
			public void run() {
				RandomAccessFile acf = null;
				try {
					state.getTask().setSend(state.getTask().getIndexs().get(new Long(state.getLastlinenum())));
					acf = new RandomAccessFile(((FileTask)state.getTask()).getContent(), "rw");
					int size = state.getTask().getIndexs().size();
					Long[] values = new Long[size];
					state.getTask().getIndexs().values().toArray(values);
					int lastLine = state.getLastlinenum()-1;
					Long value;
					while((value = state.getTask().getIndexs().get(new Long(lastLine)))!=null){
						long skip = value.longValue();
						acf.seek(skip);
						int command = acf.read();
						if(command == 155){
							byte[] bytes = new byte[4];
							float[] position = new float[3];
							acf.read(bytes);
							position[0] = (float) (ByteUtils.byte2int(bytes) / manager.machine.axis_x.steps_per_mm);
							acf.read(bytes);
							position[1] = (float) (ByteUtils.byte2int(bytes) / manager.machine.axis_y.steps_per_mm);
							acf.read(bytes);
							position[2] = (float) (ByteUtils.byte2int(bytes) / manager.machine.axis_z.steps_per_mm);
							LogUtil.d(TAG, "pause position: "+position[0]+" "+position[1]+" "+position[2]);
							manager.insertCommand("G1 X"+(position[0]-20)+" Y"+(position[1]-20)+" Z"+(position[2]+10));
							sqliteHelper.addTask((FileTask)state.getTask(), (position[0]-20)+","+(position[1]-20)+","+(position[2]+10));
							break;
						}
						lastLine --;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					if(acf!=null){
						try {
							acf.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		});
	}

	/**
	 * resume machine state from sqlitedatabase
	 */
	public static void resumeState(){
		taskHandler.post(new Runnable() {
			
			public void run() {
				FileTask task = sqliteHelper.getTask();
				if(task!=null){
					task.setTotal(task.getContent().length());
					task.setStartTime(System.currentTimeMillis());
					String[] coordinate = sqliteHelper.getCoordinate(task.getTaskName());
					sqliteHelper.deleteTask();
//					execBlockedGcode("G92 X"+coordinate[0]+" Y"+coordinate[1]+" Z"+coordinate[2]+"\nG130 X"+task.getVoltage()[0]+" Y"+task.getVoltage()[1]+" Z"+task.getVoltage()[2]+" A"+task.getVoltage()[3]+" B"+task.getVoltage()[4]+"\nM221 S"+task.getSpeedFactor()+"\nM109 T0 S"+task.getTargetTemper(0));
					x3gHandler.post(new Runnable() {
						public void run() {
							stm32.sendData(CLEAR_LINE_NUM);
							lastLineOverNum = 0;
							LogUtil.d(TAG, "clear line num");
						}
					});
					state.setTask(task);
					state.setPausing(true);
				}
			}
		});
		
	}
	
	public static void destory() {
		if (x3gHandler != null) {
			x3gHandler.getLooper().quitSafely();
			x3gHandler = null;
		}
		if (taskHandler != null) {
			taskHandler.getLooper().quitSafely();
			taskHandler = null;
		}
	}
}
